import express, { Request, Response, NextFunction } from 'express';
import { Router } from 'express';
const router:Router = express.Router();
import * as bodyParser from 'body-parser';

let apiMiddlewares = [
    bodyParser.json(), // this is to convert raw request body into json format so that it can be accessible in req.body
    function(req:Request,res:Response,next:NextFunction){
        console.log('inside apiRoutes start middleware.');
        next();
    }
];

// here if we are doing ajax calls from front end for CRUD on source, we can have REST ful router.get('source'), put,delete


router.get('/api/testroute',apiMiddlewares,   function(req:Request,res:Response,next:NextFunction){
    res.json({
        'testkey': 'testValue'
    });
});


export default router;
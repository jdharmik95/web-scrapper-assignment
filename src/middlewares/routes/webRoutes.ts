import express, { Request, Response, NextFunction } from 'express';
import { Router } from 'express';
const router:Router = express.Router();
import {displayHome} from '@controllers/home';
import {getSource,saveSource,deleteSource} from '@controllers/source';
import bodyParser from 'body-parser';
import { isValidUrl } from '@utils/validator';
const compose = require('compose-middleware').compose;

let webRouteMiddlewares =compose([
    function(req:Request,res:Response,next:NextFunction){
        console.log('inside webRoutes start middlewa1re.');
        next();
    }
]);

router.get('/source',webRouteMiddlewares,getSource);
router.get('/',webRouteMiddlewares, displayHome);
router.get('/sources',webRouteMiddlewares, displayHome);
router.post('/source',webRouteMiddlewares,bodyParser.urlencoded({extended: false,limit: '10mb'}),saveSource);
router.post('/delete-source',webRouteMiddlewares,bodyParser.urlencoded({extended: false}),deleteSource);

// here for time constraint, i'm suing create, delete source as post requests (for doing FORM data submissions), ideally here it should be api routes which returns a json response, if we are implementing ajax calls in our frontend. and source can be resourceful REST route with CRUD

export default router;
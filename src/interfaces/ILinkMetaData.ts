export interface ILinkMetaData{
    'content-encoding'?:string,
    'content-size'?: string,
    'last-modified'?: string,
    'server'?: string,
    'caption'?: string
}
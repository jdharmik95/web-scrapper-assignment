import {isValidUrl} from './validator';

// true condition checks

test('validate url - leadsquared.com', () => {
    expect(isValidUrl('leadsquared.com')).toBe(true);
});

test('validate url - https://leadsquared.com', () => {
    expect(isValidUrl('https://leadsquared.com')).toBe(true);
});

test('validate url - www.leadsquared.com', () => {
    expect(isValidUrl('www.leadsquared.com')).toBe(true);
});

test('validate url - www.leadsquared.com//', () => {
    expect(isValidUrl('www.leadsquared.com//')).toBe(true);
});

test('validate url - www.leadsquared.com/', () => {
    expect(isValidUrl('www.leadsquared.com//')).toBe(true);
});


// false condition checks

test('validate url - www.leadsquared', () => {
    expect(isValidUrl('www.leadsquared')).toBe(false);
});


test('validate url - www/leadsquared', () => {
    expect(isValidUrl('www/leadsquared')).toBe(false);
});

test('validate url - leadsquared', () => {
    expect(isValidUrl('leadsquared')).toBe(false);
});

test('validate url - www.$leadsquared', () => {
    expect(isValidUrl('www/leadsquared')).toBe(false);
});

test('validate url - //', () => {
    expect(isValidUrl('www/leadsquared')).toBe(false);
});
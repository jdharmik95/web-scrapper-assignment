import dotenv from 'dotenv';
dotenv.config();

exports = function(key:string=''): string | undefined{
    return process.env[key];
}
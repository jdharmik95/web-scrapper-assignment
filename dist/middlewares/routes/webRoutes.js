"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var router = express_1.default.Router();
var home_1 = require("@controllers/home");
var source_1 = require("@controllers/source");
var body_parser_1 = __importDefault(require("body-parser"));
var compose = require('compose-middleware').compose;
var webRouteMiddlewares = compose([
    function (req, res, next) {
        console.log('inside webRoutes start middlewa1re.');
        next();
    }
]);
router.get('/source', webRouteMiddlewares, source_1.getSource);
router.get('/', webRouteMiddlewares, home_1.displayHome);
router.get('/sources', webRouteMiddlewares, home_1.displayHome);
router.post('/source', webRouteMiddlewares, body_parser_1.default.urlencoded({ extended: false, limit: '10mb' }), source_1.saveSource);
router.post('/delete-source', webRouteMiddlewares, body_parser_1.default.urlencoded({ extended: false }), source_1.deleteSource);
// here for time constraint, i'm suing create, delete source as post requests (for doing FORM data submissions), ideally here it should be api routes which returns a json response, if we are implementing ajax calls in our frontend. and source can be resourceful REST route with CRUD
exports.default = router;

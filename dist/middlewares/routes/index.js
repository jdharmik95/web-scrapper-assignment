"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var webRoutes_1 = __importDefault(require("./webRoutes"));
var apiRoutes_1 = __importDefault(require("./apiRoutes"));
var compose_middleware_1 = require("compose-middleware");
var arrayOfMiddlewares = [
    function (req, res, next) {
        console.log('pre middleware for routes13 - middleware');
        next();
    },
    webRoutes_1.default,
    apiRoutes_1.default,
    function (req, res, next) {
        res.status(404).send("Sorry can't find that!");
        next();
    }
];
exports.default = compose_middleware_1.compose(arrayOfMiddlewares);

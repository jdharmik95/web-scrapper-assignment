"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isValidUrl = void 0;
var urlValidatorRegex = /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
function isValidUrl(url) {
    if (url === void 0) { url = null; }
    if (!url) {
        return false;
    }
    var result = url.match(urlValidatorRegex);
    if (result) {
        return true;
    }
    return false;
}
exports.isValidUrl = isValidUrl;

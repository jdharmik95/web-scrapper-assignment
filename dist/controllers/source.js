"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteSource = exports.saveSource = exports.getSource = void 0;
var source_1 = __importDefault(require("@views/source"));
var mustache_1 = __importDefault(require("mustache"));
var source_2 = require("@collections/source");
var sourceManager_1 = require("@libs/sourceManager");
var formatter_1 = require("@libs/formatter");
var moment_1 = __importDefault(require("moment"));
var validator_1 = require("@utils/validator");
function getSource(req, res, next) {
    return __awaiter(this, void 0, void 0, function () {
        var queryParams, links, success, error, mode, source, url, url_1, sourceFromDB, isFromStore, renderedHtml, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    queryParams = req.query;
                    links = [];
                    success = '';
                    error = '';
                    mode = 'saved';
                    source = null;
                    if (queryParams['success']) {
                        success = queryParams['success'];
                    }
                    if (queryParams['error']) {
                        error = queryParams['error'];
                    }
                    if (queryParams['mode']) {
                        mode = queryParams['mode'];
                    }
                    url = queryParams['url'];
                    if (!validator_1.isValidUrl(url)) {
                        res.redirect('/?error=Invalid url is given, please check source url.');
                        return [2 /*return*/];
                    }
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 5, , 6]);
                    url_1 = formatter_1.formatUrl(queryParams['url']);
                    return [4 /*yield*/, source_2.getSpecificSource(url_1)];
                case 2:
                    sourceFromDB = _a.sent();
                    isFromStore = true;
                    source = sourceFromDB;
                    if (!(!sourceFromDB || mode == 'latest')) return [3 /*break*/, 4];
                    return [4 /*yield*/, sourceManager_1.runWebScrapper(url_1)];
                case 3:
                    source = _a.sent();
                    isFromStore = false;
                    success = 'Fetched the latest links for given source url, please click save to override/save the links in datastore.';
                    _a.label = 4;
                case 4:
                    if (source) {
                        source['lastSavedAt'] = sourceFromDB && sourceFromDB['lastSavedAt'] ? sourceFromDB['lastSavedAt'] : null;
                        if (source['lastSavedAt']) {
                            source['formattedSavedAt'] = moment_1.default.unix(source['lastSavedAt']).format('LLLL');
                        }
                    }
                    renderedHtml = mustache_1.default.render(source_1.default, {
                        success: success,
                        source: source,
                        isFromStore: isFromStore,
                        stringifiedSourceData: JSON.stringify(source)
                    });
                    // res.json(links);
                    res.send(renderedHtml);
                    return [3 /*break*/, 6];
                case 5:
                    err_1 = _a.sent();
                    error = err_1;
                    res.redirect("/?error=" + error);
                    return [3 /*break*/, 6];
                case 6: return [2 /*return*/];
            }
        });
    });
}
exports.getSource = getSource;
function saveSource(req, res, next) {
    return __awaiter(this, void 0, void 0, function () {
        var reqestBody, success, error, url, source, err_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    reqestBody = req.body;
                    success = '';
                    error = '';
                    if (!validator_1.isValidUrl(reqestBody['url'])) {
                        res.redirect('/?error=Invalid url is given, please check source url.');
                        return [2 /*return*/];
                    }
                    url = formatter_1.formatUrl(reqestBody['url']);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 4, , 5]);
                    source = JSON.parse(reqestBody.stringifiedSourceData);
                    if (!source) return [3 /*break*/, 3];
                    return [4 /*yield*/, source_2.syncSource(source.url, source.links)];
                case 2:
                    _a.sent();
                    success = 'Saved the source successfully.';
                    _a.label = 3;
                case 3: return [3 /*break*/, 5];
                case 4:
                    err_2 = _a.sent();
                    console.log('error is ');
                    console.log(err_2);
                    error = err_2;
                    return [3 /*break*/, 5];
                case 5:
                    res.redirect("/source?url=" + url + "&error=" + error + "&success=" + success);
                    return [2 /*return*/];
            }
        });
    });
}
exports.saveSource = saveSource;
function deleteSource(req, res, next) {
    return __awaiter(this, void 0, void 0, function () {
        var success, error, url, err_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    success = '';
                    error = '';
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    url = req.body.url;
                    console.log('inside deleteSource url is', url);
                    return [4 /*yield*/, source_2.deleteSource(url)];
                case 2:
                    _a.sent();
                    success = 'Deleted the source successfully.';
                    return [3 /*break*/, 4];
                case 3:
                    err_3 = _a.sent();
                    console.log('error is ');
                    console.log(err_3);
                    error = err_3;
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/, res.redirect("/?error=" + error + "&sucess=" + success)];
            }
        });
    });
}
exports.deleteSource = deleteSource;
